#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fuse.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>

//============================ macros =============================================

static const char *dirPath = "/Users/rainataputra/Documents";
char *animeku = "Animeku_";
char *ian = "IAN_";

//=========================== fuse functions ====================================
// splitter
int split_the_extension(char *path){
	int flag = 0;
	for(int i=strlen(path)-1; i>=0; i--){
		if(path[i] == '.'){
			if(flag == 1) return i;
			else flag = 1;
		}
	}
	return strlen(path);
}

// function to get "." index from path. "." is the splitter between file name and its extension
int get_dot_index(char *path){
    for(int i=strlen(path)-1;i>-1;i--){
        if(path[i] == '.') return i;
    }
    // if there's no ".", then it's a directory
    return strlen(path);
}

// function to get "/" index from path. Index after "/" is the first character from the file's name.
int get_slash_index(char *path, int end){
    for(int i=0;i<strlen(path);i++){
        if(path[i] == '/') return i+1;
    }
    // if there's no "/", then it's a directory, so we just need to return its firts index
    return end;
}

// function to rename log
void rename_wibu_log(const char *message, const char*before, const char *after){
	char buffer[1000];

	FILE *file;
	char *start = "/wibu.log";
	char path[1000]; strcpy(path, dirPath); strcat(path,start);

	// write file
	file = fopen(path,"a");
	sprintf(buffer, "%s %s/%s -> %s/%s\n",message,dirPath,before,dirPath,after);
	fputs(buffer, file);
	fclose(file);
	return;
}

void hayolongapain_log(char *str, char *type){
    char logPath[100];
    sprintf(logPath, "%s/hayolongapain_C03.log", dirPath);
	time_t t = time(NULL);
    struct tm time = *localtime(&t);
    FILE *logFile = fopen(logPath, "a");
    if (strcmp(type, "INFO") == 0)
        fprintf(logFile, "INFO::%02d%02d%04d-%02d:%02d:%02d::%s\n", time.tm_mday, time.tm_mon + 1, time.tm_year + 1900, time.tm_hour, time.tm_min, time.tm_sec, str);
    else if (strcmp(type, "WARNING") == 0 )
        fprintf(logFile, "WARNING::%02d%02d%04d-%02d:%02d:%02d::%s\n", time.tm_mday, time.tm_mon + 1, time.tm_year + 1900, time.tm_hour, time.tm_min, time.tm_sec, str);
    fclose(logFile);
}

// function to encrypt a word into atbash
void encrypt_at_bash_rot(char *path){
    if(!strcmp(path,".") || !strcmp(path,"..")) return; // if its name is "." or ".", return nothing
    int last_index = split_the_extension(path);
	if(last_index == strlen(path))
		last_index = get_dot_index(path);
	int start_index = get_slash_index(path, 0);

	char before[1000];
    for(int i=start_index;i<last_index;i++){
		before[i] = path[i];
        // check if its an alphabet. If it is, then encode
        if(path[i] != '/' && isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i])){
                temp -= 'A';
                temp = 25-temp;
            } 
            else{
                temp -= 'a';
                temp = (temp + 13) % 26;
            } 
            

            if(isupper(path[i])) temp += 'A';
            else temp += 'a';
            path[i] = temp;
        }
    }
	char *mes = "RENAME terenkripsi";
	rename_wibu_log(mes,before,path);
	printf("tes\n");
}

// function to decrypt a word into atbash
void decrypt_at_bash_rot(char *path){
	if(!strcmp(path,".") || !strcmp(path,"..")) return; // if its name is "." or ".", return nothing
	int last_index = split_the_extension(path);
	if(last_index == strlen(path))
		last_index = get_dot_index(path);
	int start_index = get_slash_index(path, last_index);

	for(int i=start_index; i<last_index; i++){
		// check if its an alphabet. If it is, then encode
        if(path[i] != '/' && isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i])){
                temp -= 'A';
                temp = 25-temp;
            } 
            else{
                temp -= 'a';
                temp = (temp + 13) % 26;
            } 
            

            if(isupper(path[i])) temp += 'A';
            else temp += 'a';
            path[i] = temp;
        }
	}
}

//function to encrypt a word into vigenere chiper
void encrypt_vigenere_chiper(char *str){
	if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0) return;

    int str_length = strlen(str);
    char key[] = "INNUGANTENG";
    int key_length = strlen(key);
    char new_key[str_length];
    //new key
    for(int i=0, j=0; i<str_length; i++, j++){
        if(j == key_length) j = 0;
        new_key[i] = key[j];
    }
	new_key[str_length] = '\0';
    for(int i=0, j=0; i<str_length; i++, j++){
        if (islower(str[i])) str[i] = tolower(((toupper(str[i]) + new_key[j]) % 26) + 'A');
        else if (isupper(str[i])) str[i] = ((str[i] + new_key[j]) % 26) + 'A';
        else j--;
    }
}

//function to decrypt a word into vigenere chiper
void decrypt_vigenere_chiper(char *str, int flag){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0 || strstr(str, "/") == NULL) return;
    
	int str_length = strlen(str), start_index = 0, end_index = str_length;
    for (int i = 0; i<str_length; i++){
        if (str[i] == '/'){
            start_index = i+1;
            break;
        }
    }

    if (flag == 1){
        for (int i=end_index-1; i>-1; i--){
            if (str[i] == '/'){
                end_index = i;
                break;
            }
        }
    }

    char key[] = "INNUGANTENG";
    int key_length = strlen(key);
    char new_key[str_length];
    //new key
    for(int i=start_index, j=0; i<end_index; i++, j++){
        if(j == key_length) j = 0;
        new_key[i] = key[j];
    }
	new_key[str_length] = '\0';
    for(int i=start_index, j=start_index; i<end_index; i++, j++){
		if (str[i] == '/') j = start_index-1;
        else if (islower(str[i])) str[i] = tolower((((toupper(str[i]) - new_key[j]) + 26) % 26) + 'A');
        else if (isupper(str[i])) str[i] = (((str[i] - new_key[j]) + 26) % 26) + 'A';
        else j--;
    }
}

// ==========
int cekEnkrip(char *fpath){
	char temp[10000];
	strcpy(temp,fpath);

	char *token = strtok(temp,"/");
	while(token != NULL){
		if(strncmp("AtoZ_",token,5) == 0){
			return 1;
		}
		if(!strncmp("RX_",token,3)){
			return 2;
		}
		if(!strncmp("nam_do-saq_",token,7)){
			return 3;
		}
		token = strtok(NULL,"/");
	}
	return 0;
} 

int tipe(const char* path) {
    struct stat statbuf; 
    if (stat(path, &statbuf) == -1)
        return -1;
    return S_ISDIR(statbuf.st_mode);
}
 
char *special(char *filename){
    int length_extension = 0;
    char *extension = strrchr(filename,'.');
    if(extension){
        length_extension = strlen(extension);
    }
 
    int pengkali = 1;
    int total = 0;
    for(int  i = strlen(filename) - length_extension - 1; i >= 0; i--){
        if(filename[i] >= 'A' && filename[i] <= 'Z'){
            filename[i] = 'a' + (filename[i] - 'A');
            total += pengkali;
        }
        pengkali = pengkali << 1;
    }
 
    sprintf(filename,"%s.%d",filename,total);
    return filename;
}
 
char *dec_special(char *filename){
    int length_decimal = 0,angka = 0;
    char *decimal = strrchr(filename,'.');
    if(decimal){
        length_decimal = strlen(decimal);
		angka = atoi(decimal+1);
    }
 
    char temp[1000] = {0};
    strncpy(temp,filename,strlen(filename)-length_decimal);
    temp[strlen(filename)-length_decimal]='\0';
 
    int length_extension = 0;
    char *extension = strrchr(temp,'.');
    if(extension){
        length_extension = strlen(extension);
    }
 
    int ctr = 0;
    for(int i = strlen(temp) - length_extension - 1; i >= 0 ; i--){
        if((1 << ctr) & angka){
            temp[i] = 'A' + temp[i] - 'a';
        }
        ctr++;
    }
    strcpy(filename,temp);
    return filename;
}

char *enkripsi_atbash(char *filename){
	int length_extension = 0;
	char *extension = strrchr(filename,'.');
	if(extension){
		length_extension = strlen(extension);
	}
	for(int i = 0; i < strlen(filename)-length_extension; i++){
 
		if(filename[i] >= 'a' && filename[i] <= 'z'){
			filename[i] = 'z' + 'a' - filename[i];
		}	
		if(filename[i] >= 'A' && filename[i] <= 'Z'){
			filename[i] = 'Z' + 'A' - filename[i];
		}
 
	}
	return filename;
}

char *dec_vigenere(char *encryptedMsg){

    char key[] = "SISOP";
    int msgLen = strlen(encryptedMsg), keyLen = strlen(key), i, j,length_extension = 0;
    char newKey[msgLen], decryptedMsg[msgLen];

	char *extension = strrchr(encryptedMsg,'.');
	if(extension){
		length_extension = strlen(extension);
	}

    for(i = 0, j = 0; i < msgLen-length_extension; ++i, ++j){
        if(j == keyLen)
            j = 0;
 
        newKey[i] = key[j];
    }
    
    //decryption
    for(i = 0; i < msgLen-length_extension; ++i){
         if(encryptedMsg[i] >= 'a' && encryptedMsg[i] <= 'z')
            decryptedMsg[i] = (((encryptedMsg[i] - newKey[i]) + 26 - ('a' - 'A')) % 26) + 'a';
        else if(encryptedMsg[i] >= 'A' && encryptedMsg[i] <= 'Z')
            decryptedMsg[i] = (((encryptedMsg[i] - newKey[i]) + 26) % 26) + 'A';
        else 
            decryptedMsg[i] = encryptedMsg[i]; 
        // decryptedMsg[i] = (((encryptedMsg[i] - newKey[i]) + 26) % 26) + 'A';
    }
 
	for(;i<msgLen;++i){
        decryptedMsg[i] = encryptedMsg[i];
    }

    decryptedMsg[i] = '\0';

	strcpy(decryptedMsg,enkripsi_atbash(decryptedMsg));

    strcpy(encryptedMsg,decryptedMsg);
	return encryptedMsg;
}

char *vigenere(char *msg){

	strcpy(msg,enkripsi_atbash(msg));

    char key[] = "SISOP";
    int msgLen = strlen(msg), keyLen = strlen(key), i, j,length_extension = 0;

	char *extension = strrchr(msg,'.');
	if(extension){
		length_extension = strlen(extension);
	}
 
    char newKey[msgLen], encryptedMsg[msgLen];
 
    //generating new key
    for(i = 0, j = 0; i < msgLen; ++i, ++j){
        if(j == keyLen)
            j = 0;
 
        newKey[i] = key[j];
    }

    newKey[i] = '\0';
 
    //encryption
    for(i = 0; i < msgLen - length_extension; ++i){
        if(msg[i] >= 'a' && msg[i] <= 'z')
            encryptedMsg[i] = ((msg[i] + newKey[i] - ('a' - 'A')) % 26) + 'a';
        else if(msg[i] >= 'A' && msg[i] <= 'Z')
            encryptedMsg[i] = ((msg[i] + newKey[i]) % 26) + 'A';
        else
            encryptedMsg[i] = msg[i];
    }

	for(;i<msgLen;++i){
        encryptedMsg[i] = msg[i];
    }

    encryptedMsg[i] = '\0';
    strcpy(msg,encryptedMsg);
	return msg;
}

char *rot13(char *filename){
	strcpy(filename,enkripsi_atbash(filename));

	int length_extension = 0;

	char *extension = strrchr(filename,'.');

	if(extension){
		length_extension = strlen(extension);
	}

	for(int i = 0; i < strlen(filename) - length_extension; i++){
 
		if(filename[i] >= 'a' && filename[i] <= 'z'){
			filename[i] += 13;
			if(filename[i] > 'z'){
				filename[i] -= 26;
			}
		}	
		if(filename[i] >= 'A' && filename[i] <= 'Z'){
			filename[i] += 13;
			if(filename[i] > 'Z'){
				filename[i] -= 26;
			}
		}
	}
	return filename;
}

void rekursi(char fpath[1000],int jenis){
	if(!strlen(fpath)){
		return;
	}
 
    DIR *d;
    struct dirent *dir;
 
    d = opendir(fpath);
    if (!d){
        return;
    }
    while((dir = readdir(d)) != NULL){
 
        if(!strcmp(dir->d_name,".")||!strcmp(dir->d_name,"..")){
            continue;
        }
 
		char n_name[100],tmp[100],path[100];
		strcpy(tmp,dir->d_name);

		if(jenis == 1)
			sprintf(n_name,"%s/%s",fpath,enkripsi_atbash(tmp));
		else if(jenis == 2)
			sprintf(n_name,"%s/%s",fpath,vigenere(tmp));
		else if(jenis == 3)
			sprintf(n_name,"%s/%s",fpath,dec_vigenere(tmp));
		else if(jenis == 4)
			sprintf(n_name,"%s/%s",fpath,rot13(tmp));

		strcpy(path,fpath);
		strcat(path,"/");
		strcat(path,dir->d_name);
 
		rename(path,n_name);
 
        if(tipe(n_name)==1){
            rekursi(n_name,jenis);
            continue;
        }
 
    }
    closedir(d);
}
 
 
char *proses(char *filename){
	char *ok;
    ok = strchr(filename,'/') + 1;
 
    while (ok != NULL){ 
		if(!strncmp(ok,"AtoZ_",5)){		
			if(strchr(ok,'/')){
				ok = strchr(ok,'/') + 1;
				int ind = ok - filename;
				enkripsi_atbash(ok);
				for(int i = 0; i < strlen(ok); i++){
					filename[ind + i] = ok[i]; 
				}
				filename[ind+strlen(ok)] = '\0';
				break;
			}
		}

		if(!strncmp(ok,"RX_",3)){
			if(strchr(ok,'/')){
				ok = strchr(ok,'/') + 1;
				int ind = ok - filename;
				// cekDua(filename)
				int mana = 0;

				if(mana == 3){
					dec_vigenere(ok);
				}
				if(mana == 4){
					rot13(ok);
				}

				for(int i = 0; i < strlen(ok); i++){
					filename[ind + i] = ok[i]; 
				}
				filename[ind+strlen(ok)] = '\0';
				break;
			}
		}


		if(!strncmp(ok,"nam_do-saq_",7)){
			if(strchr(ok,'/')){
				ok = strchr(ok,'/') + 1;
				int ind = ok - filename;

				dec_special(ok);
				for(int i = 0; i < strlen(ok); i++){
					filename[ind + i] = ok[i]; 
				}
				filename[ind+strlen(ok)] = '\0';

				break;
			}
		}
        if(strchr(ok,'/') == NULL)
            break;
 
        ok = strchr(ok,'/') + 1;
    }
	return filename;
}


//================================================= FUSE OPERATIONS ===============================================================
static int xmp_getattr(const char *path, struct stat *stbuf){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = lstat(filepath, stbuf);
	if (result == -1) return -errno;
	return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	// if (x != 24) x++;
	// else writeTheLog("READDIR", filepath);

	int result = 0;
	DIR *dp;
	struct dirent *de;

	(void)offset;
	(void)fi;

	dp = opendir(filepath);
	if (dp == NULL) return -errno;

	while ((de = readdir(dp)) != NULL){
		if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;

		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;

		if (a != NULL) {
			encrypt_at_bash_rot(de->d_name);
			printf("%s",de->d_name);
		}
		// if (b != NULL){enkripsiMenjadiAtbash(de->d_name);enkripsiMenjadiRot13(de->d_name);}
		if (b != NULL) {
			encrypt_vigenere_chiper(de->d_name);
			printf("%s",de->d_name);
		}

		result = (filler(buf, de->d_name, &st, 0));
		if (result != 0) break;
	}

	closedir(dp);
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b); dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath; sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	int result = 0;
	int fd = 0;

	(void)fi;
	// writeTheLog("READ", filepath);

	fd = open(filepath, O_RDONLY);
	if (fd == -1) return -errno;

	result = pread(fd, buf, size, offset);
	if (result == -1) result = -errno;

	close(fd);
	return result;
}

static int xmp_mkdir(const char *path, mode_t mode){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 1);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = mkdir(filepath, mode);
	// writeTheLog("MKDIR", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "MKDIR::%s", filepath);
		hayolongapain_log(temp, "INFO");
	}

	if (result == -1) return -errno;
	return 0;
}

static int xmp_rmdir(const char *path)
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){
	// 	dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);
	// }

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = rmdir(filepath);
	// writeTheLog("RMDIR", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "REMOVE::%s", filepath);
		hayolongapain_log(temp, "WARNING");
	}

	if (result == -1) return -errno;
	return 0;
}

static int xmp_rename(const char *from, const char *to){
	int result;
	char dariDir[1000], keDir[1000];
	
	char *a = strstr(to, animeku);
	if (a != NULL) decrypt_at_bash_rot(a);
	char *b = strstr(from, ian);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	char *c = strstr(to, ian);
	if (c != NULL) decrypt_vigenere_chiper(c, 0);
	
	// char *b = strstr(from, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}
	
	// char *c = strstr(to, rx);
	// if (c != NULL){dekripsiMenjadiRot13(c);dekripsiMenjadiAtbash(c);}

	sprintf(dariDir, "%s%s", dirPath, from);
	sprintf(keDir, "%s%s", dirPath, to);

	result = rename(dariDir, keDir);
	// writeTheLog2("RENAME", dariDir, keDir);
	if (b != NULL){
		char temp[2048];
		sprintf(temp, "RENAME::%s::%s", dariDir, keDir);
		hayolongapain_log(temp, "INFO");
	}

	if (result == -1) return -errno;

	
	// if (c != NULL){lakukanEnkripsi(keDir);writeTheLog2("ENCRYPT2", from, to);}
	// if (b != NULL && c == NULL){lakukanDekripsi(keDir);writeTheLog2("DECRYPT2", from, to);}
	// if (strstr(to, aisa) != NULL){encryptBinary(keDir);writeTheLog2("ENCRYPT3", from, to);}
	// if (strstr(from, aisa) != NULL && strstr(to, aisa) == NULL){decryptBinary(keDir);writeTheLog2("DECRYPT3", from, to);}

	return 0;
}

static int xmp_truncate(const char *path, off_t size){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	// writeTheLog("TRUNC", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "TRUNC::%s", filepath);
		hayolongapain_log(temp, "INFO");
	}
	result = truncate(filepath, size);
	
	if (result == -1) return -errno;
	return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
	int fd;
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	(void)fi;
	// writeTheLog("WRITE", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "WRITE::%s", filepath);
		hayolongapain_log(temp, "INFO");
	}

	fd = open(filepath, O_WRONLY);
	if (fd == -1) return -errno;

	result = pwrite(fd, buf, size, offset);
	if (result == -1) result = -errno;

	close(fd);
	return result;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	(void)fi;
	// writeTheLog("CREATE", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "CREATE::%s", filepath);
		hayolongapain_log(temp, "INFO");
	}
	
	result = creat(filepath, mode);	
	if (result == -1) return -errno;

	close(result);
	return 0;
}
/*
static int xmp_utimens(const char *path, const struct timespec ts[2])
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	struct timeval tv[2];

	tv[0].tv_sec = ts[0].tv_sec;
	tv[0].tv_usec = ts[0].tv_nsec / 1000;
	tv[1].tv_sec = ts[1].tv_sec;
	tv[1].tv_usec = ts[1].tv_nsec / 1000;

	result = utimes(filepath, tv);
	if (result == -1) return -errno;
	return 0;
}
*/
static int xmp_access(const char *path, int mask){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = access(filepath, mask);
	if (result == -1) return -errno;
	return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = open(filepath, fi->flags);

	// writeTheLog("OPEN", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "OPEN::%s", filepath);
		hayolongapain_log(temp, "INFO");
	}

	if (result == -1) return -errno;

	close(result);
	return 0;
}

static int xmp_unlink(const char *path)
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = unlink(filepath);
	// writeTheLog("UNLINK", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "UNLINK::%s", filepath);
		hayolongapain_log(temp, "WARNING");
	}

	if (result == -1) return -errno;
	return 0;
}

static int xmp_readlink(const char *path, char *buf, size_t size)
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = readlink(filepath, buf, size - 1);
	// writeTheLog("READLINK", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "READLINK::%s", filepath);
		hayolongapain_log(temp, "INFO");
	}
	if (result == -1) return -errno;

	buf[result] = '\0';
	return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 1);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	// writeTheLog("MKNOD", filepath);
	
	if (S_ISREG(mode)){
		result = open(filepath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (result >= 0) result = close(result);
	}else if (S_ISFIFO(mode)) result = mkfifo(filepath, mode);
	else result = mknod(filepath, mode, rdev);

	if (b != NULL){
		char temp[1024];
		sprintf(temp, "MKNOD::%s", filepath);
		hayolongapain_log(temp, "INFO");
	}
	
	if (result == -1) return -errno;
	return 0;
}

static int xmp_symlink(const char *from, const char *to){
	int result;
	char dariDir[1000], keDir[1000];
	
	char *a = strstr(to, animeku);
	if (a != NULL) decrypt_at_bash_rot(a);
	char *b = strstr(from, ian);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	char *c = strstr(to, ian);
	if (c != NULL) decrypt_vigenere_chiper(c, 0);
	
	// char *b = strstr(from, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}
	
	// char *c = strstr(to, rx);
	// if (c != NULL){dekripsiMenjadiRot13(c);dekripsiMenjadiAtbash(c);}

	sprintf(dariDir, "%s%s", dirPath, from);
	sprintf(keDir, "%s%s", dirPath, to);

	result = symlink(dariDir, keDir);
	// writeTheLog2("SYMLINK", dariDir, keDir);
	if (b != NULL){
		char temp[2048];
		sprintf(temp, "SYMLINK::%s::%s", dariDir, keDir);
		hayolongapain_log(temp, "INFO");
	}
	
	if (result == -1) return -errno;
	return 0;
}

static int xmp_link(const char *from, const char *to)
{
	int result;
	char dariDir[1000], keDir[1000];
	
	char *a = strstr(to, animeku);
	if (a != NULL) decrypt_at_bash_rot(a);
	char *b = strstr(from, ian);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	char *c = strstr(to, ian);
	if (c != NULL) decrypt_vigenere_chiper(c, 0);
	
	// char *b = strstr(from, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}
	
	// char *c = strstr(to, rx);
	// if (c != NULL){dekripsiMenjadiRot13(c);dekripsiMenjadiAtbash(c);}

	sprintf(dariDir, "%s%s", dirPath, from);
	sprintf(keDir, "%s%s", dirPath, to);

	result = link(dariDir, keDir);
	// writeTheLog2("LINK", dariDir, keDir);
	if (b != NULL){
		char temp[2048];
		sprintf(temp, "LINK::%s::%s", dariDir, keDir);
		hayolongapain_log(temp, "INFO");
	}

	if (result == -1) return -errno;
	return 0;
}

static int xmp_chmod(const char *path, mode_t mode){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = chmod(filepath, mode);
	// writeTheLog("CHMOD", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "CHMOD::%s", filepath);
		hayolongapain_log(temp, "INFO");
	}

	if (result == -1) return -errno;
	return 0;
}

static int xmp_chown(const char *path, uid_t uid, gid_t gid)
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = lchown(filepath, uid, gid);
	// writeTheLog("CHOWN", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "CHOWN::%s", filepath);
		hayolongapain_log(temp, "INFO");
	}
	
	if (result == -1) return -errno;
	return 0;
}

static int xmp_statfs(const char *path, struct statvfs *stbuf)
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	
	// char *b = strstr(path, rx);
	// if (b != NULL){dekripsiMenjadiRot13(b);dekripsiMenjadiAtbash(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = statvfs(filepath, stbuf);
	// writeTheLog("STATFS", filepath);
	if (b != NULL){
		char temp[1024];
		sprintf(temp, "STATFS::%s", filepath);
		hayolongapain_log(temp, "INFO");
	}
	
	if (result == -1) return -errno;
	return 0;
}
//========================================================================================================================================
//================================================= FUSE OPERATIONS XMP ===============================================================
static struct fuse_operations xmp_oper = {
	.getattr = xmp_getattr,
	.readdir = xmp_readdir,
	.read = xmp_read,
	.mkdir = xmp_mkdir,
	.rmdir = xmp_rmdir,
	.rename = xmp_rename,
	.truncate = xmp_truncate,
	.write = xmp_write,
	.create = xmp_create,
	// .utimens = xmp_utimens,
	.access = xmp_access,
	.open = xmp_open,
	.unlink = xmp_unlink,
	.readlink = xmp_readlink,
	.mknod = xmp_mknod,
	.symlink = xmp_symlink,
	.link = xmp_link,
	.chmod = xmp_chmod,
	.chown = xmp_chown,
	.statfs = xmp_statfs,
};
//===================================================================================================================================================

int main(int argc, char*argv[]){
    // char *path = "Animeku_/anya_FORGER.txt";
    // int dot_index = get_dot_index(path);
    // int slash_index = get_slash_index(path);

    // printf("before encoding %s\n", path);
    // printf("after encoding %s\n", encrypt_at_bash_rot(path));
    umask(0);
    return fuse_main(argc,argv,&xmp_oper,NULL);
}