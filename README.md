# soal-shift-sisop-modul-4-C03-2022



# KELOMPOK C03

```
Nama    : Ida Bagus Kade Rainata Putra Wibawa
Nrp     : 5025201235

Nama    : Moh. Ilham Fakhri Zamzami
Nrp     : 5025201275

Nama    : Ichsanul Aulia
Nrp     : 05111840007001
```

**Kendala:**
1. Seluruh soal susah dimengerti.
2. Pemberian soal di waktu ramadhan yang menyebabkan suasana pengerjaan soal kurang kondusif.

## Soal 1

1. Membuat fungsi untuk meng-encode dan meng-decode atbash (untuk huruf besar) dan rot13 (untuk huruf kecil). Atbash hanyalah kebalikan dari suatu huruf tersebut dan rot13 adalah huruf ke-13 setelah huruf awal (mundur 13 urutan huruf > 13). Untuk kode fungsinya adalah sebagai berikut:

**Fungsi pemisah extention**
```c
// splitter
int split_the_extension(char *path){
	int flag = 0;
	for(int i=strlen(path)-1; i>=0; i--){
		if(path[i] == '.'){
			if(flag == 1) return i;
			else flag = 1;
		}
	}
	return strlen(path);
}
```
**Fungsi mendapatkan index karakter "." (dot)**
```c
// function to get "." index from path. "." is the splitter between file name and its extension
int get_dot_index(char *path){
    for(int i=strlen(path)-1;i>-1;i--){
        if(path[i] == '.') return i;
    }
    // if there's no ".", then it's a directory
    return strlen(path);
}
```
**Fungsi mendapatkan index karakter "/" (slash)**
```c
// function to get "/" index from path. Index after "/" is the first character from the file's name.
int get_slash_index(char *path, int end){
    for(int i=0;i<strlen(path);i++){
        if(path[i] == '/') return i+1;
    }
    // if there's no "/", then it's a directory, so we just need to return its firts index
    return end;
}
```


``` c
// function to encrypt a word into atbash
void encrypt_at_bash_rot(char *path){
    if(!strcmp(path,".") || !strcmp(path,"..")) return; // if its name is "." or ".", return nothing
    int last_index = split_the_extension(path);
	if(last_index == strlen(path))
		last_index = get_dot_index(path);
	int start_index = get_slash_index(path, 0);

	char before[1000];
    for(int i=start_index;i<last_index;i++){
		before[i] = path[i];
        // check if its an alphabet. If it is, then encode
        if(path[i] != '/' && isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i])){
                temp -= 'A';
                temp = 25-temp;
            } 
            else{
                temp -= 'a';
                temp = (temp + 13) % 26;
            } 
            

            if(isupper(path[i])) temp += 'A';
            else temp += 'a';
            path[i] = temp;
        }
    }
	char *mes = "RENAME terenkripsi";
	rename_wibu_log(mes,before,path);
	// printf("tes\n");
}

// function to decrypt a word into atbash
void decrypt_at_bash_rot(char *path){
	if(!strcmp(path,".") || !strcmp(path,"..")) return; // if its name is "." or ".", return nothing
	int last_index = split_the_extension(path);
	if(last_index == strlen(path))
		last_index = get_dot_index(path);
	int start_index = get_slash_index(path, last_index);

	for(int i=start_index; i<last_index; i++){
		// check if its an alphabet. If it is, then encode
        if(path[i] != '/' && isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i])){
                temp -= 'A';
                temp = 25-temp;
            } 
            else{
                temp -= 'a';
                temp = (temp + 13) % 26;
            } 
            

            if(isupper(path[i])) temp += 'A';
            else temp += 'a';
            path[i] = temp;
        }
	}
}
```
Tentu saja proses-proses diatas menggunakan library **fuse.h** yang pada kasus ini kami menggunakan fungsi-fungsi berikut:
```c
//================================================= FUSE OPERATIONS XMP ===============================================================
static struct fuse_operations xmp_oper = {
	.getattr = xmp_getattr,
	.readdir = xmp_readdir,
	.read = xmp_read,
	.mkdir = xmp_mkdir,
	.rmdir = xmp_rmdir,
	.rename = xmp_rename,
	.truncate = xmp_truncate,
	.write = xmp_write,
	.create = xmp_create,
	.utimens = xmp_utimens,
	.access = xmp_access,
	.open = xmp_open,
	.unlink = xmp_unlink,
	.readlink = xmp_readlink,
	.mknod = xmp_mknod,
	.symlink = xmp_symlink,
	.link = xmp_link,
	.chmod = xmp_chmod,
	.chown = xmp_chown,
	.statfs = xmp_statfs,
};
```
Proses encoding terjadi pada fungsi **xmp_readdir** yang isinya sebagai berikut:
``` c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
	char filepath[1000];
	
	char *a = strstr(path, animeku);
	if (a != NULL) decrypt_at_bash_rot(a);
	
	// kode lain

	while ((de = readdir(dp)) != NULL){
		if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;

		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;

		if (a != NULL) {
            // encode
			encrypt_at_bash_rot(de->d_name);
			printf("%s",de->d_name);
		}
		// if (b != NULL){enkripsiMenjadiAtbash(de->d_name);enkripsiMenjadiRot13(de->d_name);}

		result = (filler(buf, de->d_name, &st, 0));
		if (result != 0) break;
	}

	closedir(dp);
	return 0;
}
```
2. Proses rename (soal b) dengan awalan **"Animeku"** isinya akan terdecode juga. Proses ini sebenarnya sama saja dengan nomor 1, hanya saja dibagian fungsi **xmp_rename**, terdapat bagian yang mendeteksi perubahan tersebut. Bisa dilihat pada potongan kode xmp_rename berikut:
```c
static int xmp_rename(const char *from, const char *to){
	int result;
	char dariDir[1000], keDir[1000];
	
	// proses cek perubahan menjadi animeku
	char *a = strstr(to, animeku);
	if (a != NULL) decrypt_at_bash_rot(a);
	
	// kode lain

	return 0;
}
```

3. Directory yang tidak ter-encode isinya akan ter-decode (soal c). Hal ini bisa dilihat dari pemanggilan fungsi **decrypt_at_bash_rot** yang terjadi terus-menerus apabila proses encode tidak terjadi.

4. Setiap data yang ter-encode akan masuk ke file **wibu.log**. Proses ini cukup sederhana, hanya perlu membuat file **wibu.log** dan menuliskan pesan yang diminta. Proses penulisan ini dijalankan oleh fungsi yang bernama **rename_wibu_log**. Isi fungsinya adalah sebagai berikut:

```c
// function to rename log
void rename_wibu_log(const char *message, const char*before, const char *after){
	char buffer[1000];

	FILE *file;
	char *start = "/wibu.log";
	char path[1000]; strcpy(path, dirPath); strcat(path,start);

	// write file
	file = fopen(path,"a");
	sprintf(buffer, "%s %s/%s -> %s/%s\n",message,dirPath,before,dirPath,after);
	fputs(buffer, file);
	fclose(file);
	return;
}
```
Kemudian, fungsi ini dipanggil di fungsi **encrypt_at_bash_rot**.
``` c
void encrypt_at_bash_rot(char *path){
    // kode lain

	char *mes = "RENAME terenkripsi";
    //pemanggilan rename
	rename_wibu_log(mes,before,path);
	// printf("tes\n");
}
```

## Screenshots

**Folder Animeku_ isinya ter-encode**

- Before
![Screen_Shot_2022-05-14_at_17.56.36](/uploads/6dff8a2eb01add0f91c9de207605ba7b/Screen_Shot_2022-05-14_at_17.56.36.png)
- After
![Screen_Shot_2022-05-14_at_17.57.47](/uploads/4b542b45db6b4682d5f3d0baeb119d3a/Screen_Shot_2022-05-14_at_17.57.47.png)

**Rename folder Photos menjadi Animeku_Photos akan meng-encode isinya**

- Before

![Screen_Shot_2022-05-14_at_17.58.41](/uploads/05dedaef71c839a4f68537e1678ec130/Screen_Shot_2022-05-14_at_17.58.41.png)
- After

![Screen_Shot_2022-05-14_at_17.59.15](/uploads/343993fa661038bae6bbf4a10f38fe53/Screen_Shot_2022-05-14_at_17.59.15.png)

**Isi dari file wibu.log**
![Screen_Shot_2022-05-14_at_17.59.37](/uploads/46b3416c9ea264f3b9d30a1ba17f7895/Screen_Shot_2022-05-14_at_17.59.37.png)


## Soal 2

1. membuat fungsi untuk meng-encode dan de-code vigenere chiper, kode fungsinya sebagai berikut :

**fungsi encode**
```c
//function to encrypt a word into vigenere chiper
void encrypt_vigenere_chiper(char *str){
	if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0) return;

    int str_length = strlen(str);
    char key[] = "INNUGANTENG";
    int key_length = strlen(key);
    char new_key[str_length];
    //new key
    for(int i=0, j=0; i<str_length; i++, j++){
        if(j == key_length) j = 0;
        new_key[i] = key[j];
    }
	new_key[str_length] = '\0';
    for(int i=0, j=0; i<str_length; i++, j++){
        if (islower(str[i])) str[i] = tolower(((toupper(str[i]) + new_key[j]) % 26) + 'A');
        else if (isupper(str[i])) str[i] = ((str[i] + new_key[j]) % 26) + 'A';
        else j--;
    }
}
```
**fungsi decode**
```c
//function to decrypt a word into vigenere chiper
void decrypt_vigenere_chiper(char *str, int flag){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0 || strstr(str, "/") == NULL) return;
    
	int str_length = strlen(str), start_index = 0, end_index = str_length;
    for (int i = 0; i<str_length; i++){
        if (str[i] == '/'){
            start_index = i+1;
            break;
        }
    }

    if (flag == 1){
        for (int i=end_index-1; i>-1; i--){
            if (str[i] == '/'){
                end_index = i;
                break;
            }
        }
    }

    char key[] = "INNUGANTENG";
    int key_length = strlen(key);
    char new_key[str_length];
    //new key
    for(int i=start_index, j=0; i<end_index; i++, j++){
        if(j == key_length) j = 0;
        new_key[i] = key[j];
    }
	new_key[str_length] = '\0';
    for(int i=start_index, j=start_index; i<end_index; i++, j++){
		if (str[i] == '/') j = start_index-1;
        else if (islower(str[i])) str[i] = tolower((((toupper(str[i]) - new_key[j]) + 26) % 26) + 'A');
        else if (isupper(str[i])) str[i] = (((str[i] - new_key[j]) + 26) % 26) + 'A';
        else j--;
    }
}
```
Lanjutannya pada setiap xmp function diberikan code sbb :
```c
...
	char *a = strstr(path, animeku);
	char *b = strstr(path, ian);
	if (a != NULL) decrypt_at_bash_rot(a);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
...
```
ataupun
```c
	char *a = strstr(to, animeku);
	if (a != NULL) decrypt_at_bash_rot(a);
	char *b = strstr(from, ian);
	if (b != NULL) decrypt_vigenere_chiper(b, 0);
	char *c = strstr(to, ian);
	if (c != NULL) decrypt_vigenere_chiper(c, 0);
```
sesuai kebutuhan dari pemanggilan fungsinya, hal ini agar nomer 1 dan nomer 2 bisa digunakan beriringan sesuai prefix nama folder yang digunakan

2. untuk kebutuhan hayolongapain.log, digunakan code sbb : 
```c
void hayolongapain_log(char *str, char *type){
    char logPath[100];
    sprintf(logPath, "%s/hayolongapain_C03.log", dirPath);
	time_t t = time(NULL);
    struct tm time = *localtime(&t);
    FILE *logFile = fopen(logPath, "a");
    if (strcmp(type, "INFO") == 0)
        fprintf(logFile, "INFO::%02d%02d%04d-%02d:%02d:%02d::%s\n", time.tm_mday, time.tm_mon + 1, time.tm_year + 1900, time.tm_hour, time.tm_min, time.tm_sec, str);
    else if (strcmp(type, "WARNING") == 0 )
        fprintf(logFile, "WARNING::%02d%02d%04d-%02d:%02d:%02d::%s\n", time.tm_mday, time.tm_mon + 1, time.tm_year + 1900, time.tm_hour, time.tm_min, time.tm_sec, str);
    fclose(logFile);
}
```
panggilan dari variabel `str` berupa kegiatan system yang merupakan system call seperti rename, mkdir, atau apapun itu dan filepathnya (desc), kemudian `type` adalah level log yang tertera, apakah itu WARNING ataupun hanya INFO saja.


## Soal 3



